import kivy

from kivy.app import App
from kivy.uix.floatlayout import FloatLayout

class MyLayout(FloatLayout):
    def change_text(self, text):
        self.ids.w_textlabel.text=text
    def increase_text_size(self):
        self.ids.w_textlabel.font_size+=1
    def decrease_text_size(self):
        self.ids.w_textlabel.font_size-=1
    

class Example2App(App):
    def build(self):
        return MyLayout()

if __name__=="__main__":
    Example2App().run()