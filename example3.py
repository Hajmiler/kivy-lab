import kivy

from kivy.app import App
from kivy.uix.floatlayout import FloatLayout

class MyLayout(FloatLayout):
    _value= 0
    def increase_num(self):
        self._value+=1
        self.ids.w_value.text=str(self._value)
    
    def decrease_num(self):
        self._value-=1
        self.ids.w_value.text=str(self._value)

class Example3App(App):
    def build(self):
        return MyLayout()

if __name__=="__main__":
    Example3App().run()