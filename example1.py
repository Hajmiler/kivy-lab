import kivy
kivy.require('1.7.0')

from kivy.app import App
from kivy.uix.floatlayout import FloatLayout

class MyLayout(FloatLayout):
    pass

class Example1App(App):
    def build(self):
        return MyLayout()

if __name__=="__main__":
    Example1App().run()